const Redis = require("ioredis");
const redis = new Redis();
const subscriber = Redis.createClient();
const express = require("express");
const app = express();
const port = 3001;

subscriber.on("message", (err, message) => {
  console.log(message);
});
subscriber.subscribe("ch-1");

app.get("/", (req, res) => {
  res.json({ message: "Subscriber 1" });
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
