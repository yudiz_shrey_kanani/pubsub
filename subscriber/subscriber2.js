const Redis = require("ioredis");
const redis = new Redis();
const subscriber = Redis.createClient();
const express = require("express");
const app = express();
const port = 3002;

subscriber.on("message", (err, message) => {
  console.log(message);
});
subscriber.subscribe("ch-2");

app.get("/", (req, res) => {
  res.json({ message: "Subscriber 2" });
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
