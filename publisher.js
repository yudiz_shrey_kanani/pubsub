const Redis = require("ioredis");
const redis = new Redis();
const publisher = Redis.createClient();
const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  publisher.publish("ch-1", "hello from ch-1", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(data);
    }
  });
  res.json({ message: "Message published" });
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
